# Projekt gry Lotto stworzony w ramach Hackatonu HackeYeah.

Wybrane przez zespół zadanie miało następująca treść:

>Enter the world of entertainment. Totalizator Sportowy - the owner of the LOTTO brand invites you to design a new game. The world of lotteries absorbs more and more solutions from the area of new technologies. It is important to achieve synergy between many communication channels and to build multidimensional tools to broaden the horizons of entertainment.

>We are looking for a game that combines geolocation with randomness. Maps, coordinates, luck, win. The product of your work should reward players for their activity and gaining new users. It should be based on a simple message and understandable rules. Do not worry about legal restrictions on games of chance and where to get money for winnings from; what counts is creativity and inspiration. Remember that in a game of chance there are winners and those less lucky and that luck plays more important role than skills. We count on your creativity, knowledge and skills."

Prototyp i prezentację zobaczyć można w folderze "prezentacja i prototyp"