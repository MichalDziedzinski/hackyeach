let UISelectors = {
    firstView: 'first-view',
    firstViewBtn: 'login-btn',

    secondView: 'second-view',
    secondViewBtn: 'second-view__btn',

    thirdView: 'third-view',
    thirdViewBtn: 'third-view-btn',

    fourthView: 'mapContainer',
    fourthViewBtn: 'fourth-view__btn',

};

var counter1=1;


document.getElementById(UISelectors.firstViewBtn).addEventListener('click', () => {
    document.getElementById(UISelectors.firstView).style.display = "none";
    document.getElementById(UISelectors.secondView).style.display = "block"
});

document.getElementById(UISelectors.secondViewBtn).addEventListener('click', () => {
    document.getElementById(UISelectors.secondView).style.display = "none";
    document.getElementById(UISelectors.thirdView).style.display = "block"
});

document.getElementById(UISelectors.thirdViewBtn).addEventListener('click', () => {

    document.getElementById(UISelectors.thirdView).style.display = "none";
    document.getElementById(UISelectors.fourthView).style.display = "block";
});

document.getElementById(UISelectors.fourthViewBtn).addEventListener('click', () => {

    document.getElementById(UISelectors.fourthView).style.display = "none";
    document.getElementById(UISelectors.thirdView).style.display = "block";
});


