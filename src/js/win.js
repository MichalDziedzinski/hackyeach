import drawPath from './map';
import {
    fromLatLon
} from './converter'

var currentLatitude;
var currentLongtitude;
var numberToRound = 1000000;
var utm;
var newEasting;
var newNorthing;
var counter1 = 1;
var coArray = [];
var howManyGames = 1;
var dLengthBefore;
var dLength;
var winningPrize;

function watchLocation(successCallback, errorCallback) {
    // console.log(coords.latitude);
    successCallback = successCallback || function () {};
    errorCallback = errorCallback || function () {};
    // Try HTML5-spec geolocation.
    var geolocation = navigator.geolocation;
    if (geolocation) {
        // We have a real geolocation service.
        try {
            function handleSuccess(position) {
                successCallback(position.coords);
            }
            geolocation.watchPosition(handleSuccess, errorCallback, {
                enableHighAccuracy: true,
                maximumAge: 5000 // 5 sec.
            });
        } catch (err) {
            errorCallback();
            // document.getElementById('err').innerHTML = 'dupa1';
        }
    } else {
        errorCallback();
        // document.getElementById('err').innerHTML = 'dupa2';
    }
}

function generateRandomPoint() {
    newEasting = utm.easting + Math.floor((Math.random() * 1001) - 499);
    newNorthing = utm.northing + Math.floor((Math.random() * 1001) - 499);

    // console.log(utm.easting);
    // console.log(newEasting);
    // console.log(utm.easting - newEasting);
    // console.log("north");
    // console.log(utm.northing);
    // console.log(newNorthing);
    // console.log(utm.northing - newNorthing);
    // console.log(newEasting);
    // console.log(toLatLon(newEasting, newNorthing , 34, "U"));
    isInCircle(utm.easting, utm.northing, newEasting, newNorthing, 500)
}

function isInCircle(a, b, x, y, r) {
    var dist_points = (a - x) * (a - x) + (b - y) * (b - y);
    r = r * r;
    if (dist_points <= r) {
        // return true;
        coArray.push([x, y]);
        console.log('Spełnione', coArray);
        howManyGames++;
    } else {
        // newEasting = utm.easting + Math.floor((Math.random() * 1001) - 499);
        // newNorthing = utm.northing + Math.floor((Math.random() * 1001) - 499);
        console.log('NIespełnione', coArray);
        generateRandomPoint();
    }
    // console.log(isInCircle(utm.easting, utm.northing, newEasting, newNorthing, 500));
}

function howFarToWin(a1, b1, c1, d1) {
    dLengthBefore = ((a1 - b1) * (a1 - b1)) + ((c1 - d1) * (c1 - d1));
    dLength = Math.sqrt(dLengthBefore);
    console.log(dLength);
    if (dLength>200){
        winningPrize = 1;
    }
    else if (dLength>100 && dLength<=200) {
        winningPrize = 2;
    }
    else if (dLength>50 && dLength <=100) {
        winningPrize = 5;
    }
    else if (dLength>25 && dLength <= 15) {
        winningPrize = 100;
    }
    else if (dLength<15 && dLength<=10) {
        winningPrize = 250;
    }
    else if (dLength<10 && dLength>1) {
        winningPrize = 1000;
    }
    else if (dLength<=1) {
        winningPrize = 5000;
    }

    console.log(winningPrize);
    document.getElementById('your_win').innerHTML=winningPrize;
    document.getElementById('game_turn').innerHTML=howManyGames;
    document.getElementById('game_turn2').innerHTML=howManyGames;
}
// var  dLengthBefore=((a1-b1)(a1-b1))+((c1-d1)(c1-d1));
// var  dLength=Math.sqrt(dLengthBefore);
$(document).ready(function () {
    watchLocation(function (coords) {
        // document.getElementById('test').innerHTML = 'coords latitude: ' + coords.latitude + ', coords longitute' + coords.longitude;
        // CONTENT OF MAIN FUCTION
        // new latitude is current location of user
        currentLatitude = Math.round(coords.latitude * numberToRound) / numberToRound;
        // console.log(currentLatitude);
        currentLongtitude = Math.round(coords.longitude * numberToRound) / numberToRound;
        utm = fromLatLon(currentLatitude, currentLongtitude)
        // newEasting = utm.easting;
        // newNorthing = utm.northing;
    }, function () {
        // document.getElementById('test').innerHTML = 'error';
    });
    $("#third-view-btn").click(function () {
    // $("#mapContainer").click(function () {
        // generate new radnom point (OUR DESTINATION)
        if (howManyGames === 1) {
            generateRandomPoint();
            howFarToWin(utm.easting, newEasting, utm.northing, newNorthing);
            // drawPath();
            // document.getElementById('result').innerHTML = dLength + "metrów do celu";
        } else if (howManyGames < 6) {
            howFarToWin(utm.easting, newEasting, utm.northing, newNorthing);
            // console.log(utm.easting, newEasting, utm.northing, newNorthing);
            // document.getElementById('result').innerHTML = dLength + "metrów do celu";
            // drawPath();
            howManyGames++;
        } else {
            generateRandomPoint();
            howManyGames = 1;
            howFarToWin(utm.easting, newEasting, utm.northing, newNorthing);
            // document.getElementById('result').innerHTML = dLength + "metrów do celu";
        }
        
    });
    //end of document ready
});

export { coArray };