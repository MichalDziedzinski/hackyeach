import {
    coArray
} from './win'
import {
    toLatLon
} from './converter'
// Initialize the platform object:
const platform = new H.service.Platform({
    'app_id': '0cUfpsPpz0pyJZHSsHIC',
    'app_code': 'AzuYo-pySjm2Hebu9IwYHQ',
    useCIT: true,
    useHTTPS: true
});

let points = coArray;


// // Obtain the default map types from the platform object
// const maptypes = platform.createDefaultLayers();

// // Instantiate (and display) a map object:
// const map = new H.Map(
//     document.getElementById('mapContainer'),
//     maptypes.normal.map, {
//         zoom: 10,
//         center: {
//             lng: pointsToPush.lng,
//             lat: pointsToPush.lat,
//             // lng: 13.4,
//             // lat: 52.51
//         }
//     });

// var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));



// Initialize a linestring and add all the points to it:

let drawPath = () => {

    let transformedPoints = toLatLon(points[0][0], points[0][1], 34, "U");

    let pointsToPush = {
        lat: transformedPoints.latitude,
        lng: transformedPoints.longitude
    };


    // Instantiate (and display) a map object:
    const map = new H.Map(
        document.getElementById('mapContainer'),
        maptypes.normal.map, {
            zoom: 10,
            center: {
                lng: pointsToPush.lng,
                lat: pointsToPush.lat,
                // lng: 13.4,
                // lat: 52.51
            }
        });

    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

    var linestring = new H.geo.LineString();
    points.forEach(function (point) {
        let transformedPoint = toLatLon(point[0], point[1], 34, "U");

        let pointToPush = {
            lat: transformedPoint.latitude,
            lng: transformedPoint.longitude
        };

        linestring.pushPoint(pointToPush);
    });

    // Initialize a polyline with the linestring:
    var polyline = new H.map.Polyline(linestring, {
        style: {
            lineWidth: 10
        }
    });

    // Add the polyline to the map:
    map.addObject(polyline);

    // Zoom the map to make sure the whole polyline is visible:
    map.setViewBounds(polyline.getBounds());

    points.forEach((el) => {
        var circle = new H.map.Circle({
            lat: el.lat,
            lng: el.lng
        }, 5);
        map.addObject(circle);

    });
}

// Instantiate a circle object (using the default style):
// const drawPoint = (el, index) => {

//     // var circle = new H.map.Circle({
//     //     lat: 52.51,
//     //     lng: 13.4
//     // }, 8000);
//     // map.addObject(circle);
// }

// Add the circle to the map:
// // Create the default UI components
// var ui = H.ui.UI.createDefault(map, defaultLayers);

export default drawPath;